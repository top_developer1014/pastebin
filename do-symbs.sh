#!/bin/bash -x
ln -sf $PWD/bashrc ~/.bashrc
ln -sf $PWD/bin ~/bin
ln -sf $PWD/tmux.conf ~/.tmux.conf
ln -sf $PWD/gitconfig ~/.gitconfig
